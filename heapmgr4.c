/*--------------------------------------------------------------------*/
/* heapmgr4.c                                                         */
/* Author: Gi Hong Ann, Nicholas Liu                                  */
/*--------------------------------------------------------------------*/

#define _GNU_SOURCE

#include "heapmgr.h"
#include "checker4.h"
#include "chunk4.h"
#include <stddef.h>
#include <assert.h>
#include <unistd.h>

/*--------------------------------------------------------------------*/

/* The state of the HeapMgr. */

/* The address of the start of the heap. */
static Chunk_T oHeapStart = NULL;

/* The address immediately beyond the end of the heap. */
static Chunk_T oHeapEnd = NULL;

/* The free list is a list of all free chunks in no particular order.   */
static Chunk_T oFreeList = NULL;

/*--------------------------------------------------------------------*/

/* Request more memory from the operating system -- enough to store
   uUnits units. Insert the new chunk into the free list at the 
      front. If appropriate, coalesce the new chunk and the 
      previous one in memory.  To do so, remove the 
      current chunk from the free list, remove the 
      previous chunk in memory from the free list, 
      coalesce them to form a larger chunk, 
      and insert the larger chunk into the free list 
      at the front.  Let the current free list 
      chunk be the new chunk. */

static Chunk_T HeapMgr_getMoreMemory(Chunk_T oPrevChunk, size_t uUnits)
{
  const size_t MIN_UNITS_FROM_OS = 512;
  Chunk_T oChunk;
  Chunk_T oPrevChunk;
  Chunk_T oNewHeapEnd;
  size_t uBytes;
  /* enforce minimum new memory */
  if (uUnits < MIN_UNITS_FROM_OS)
    uUnits = MIN_UNITS_FROM_OS;

  /* Move the program break. */
  uBytes = Chunk_unitsToBytes(uUnits);
  oNewHeapEnd = (Chunk_T)((char *)oHeapEnd + uBytes);
  if (oNewHeapEnd < oHeapEnd) /* Check for overflow */
    return NULL;
  if (brk(oNewHeapEnd) == -1)
    return NULL;
  oChunk = oHeapEnd;
  oHeapEnd = oNewHeapEnd;
  oPrevChunk = Chunk_getPrevInMem(oChunk, oHeapStart);
  /* Set the fields of the new chunk. */

  Chunk_setUnits(oChunk, uUnits);
  /* Next in list -> previous first chunk */
  /* Chunk_setNextInList(oChunk, NULL); */
  /* Previous in list -> start of list */
  /* To do: Chunk_setPrevInList */

  /* Add the new chunk to the start of the free list. */
  oFreeList = oChunk;
  /* Coalesce the new chunk and the previous one if appropriate. */
  if (Chunk_getNextInMem(oPrevChunk, oHeapEnd) == oChunk)
  {
    Chunk_setUnits(oPrevChunk,
                   Chunk_getUnits(oPrevChunk) + uUnits);
    Chunk_setNextInList(oPrevChunk, NULL);
    oChunk = oPrevChunk;
  }

  return oChunk;
}

/*--------------------------------------------------------------------*/

/* If oChunk is close to the right size (as specified by uUnits),
then splice oChunk out of the free list, set its status to INUSE,
and return oChunk. If oChunk is too big, split it,insert the tail end 
of it into the free list at the front, set the status of the front end 
of it to INUSE, set the status of the tailend of it to FREE, and return
the front end of it.  */

static Chunk_T HeapMgr_useChunk(Chunk_T oChunk, size_t uUnits)
{
  Chunk_T oNewChunk;
  Chunk_T oPrevChunk;
  Chunk_T oNextChunk;
  size_t uChunkUnits;

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  uChunkUnits = Chunk_getUnits(oChunk);

  oPrevChunk = Chunk_getPrevInList(oChunk);
  oNextChunk = Chunk_getNextInList(oChunk);
  /* If oChunk is close to the right size, then use it. */
  if (uChunkUnits < uUnits + MIN_UNITS_PER_CHUNK)
  {
    /* Splice oChunk out of the list; set head to next if was head*/
    if (oPrevChunk == NULL)
      oFreeList = Chunk_getNextInList(oChunk);
    else
      /* Set previous in list to point forward to oChunk's next, */
      /* Set next in list to point back to oChunk's previous */
      Chunk_setNextInList(oPrevChunk, oNextChunk);
    Chunk_setPrevInList(oNextChunk, oPrevChunk);
    Chunk_setStatus(oChunk, CHUNK_INUSE);
    return oChunk;
  }

  /* oChunk is too big, so use the tail end of it. */
  Chunk_setUnits(oChunk, uChunkUnits - uUnits);
  oNewChunk = Chunk_getNextInMem(oChunk, oHeapEnd);
  Chunk_setUnits(oNewChunk, uUnits);
  return oNewChunk;
}

/*--------------------------------------------------------------------*/

void *HeapMgr_malloc(size_t uBytes)
{
  Chunk_T oChunk;
  Chunk_T oPrevChunk;
  Chunk_T oPrevPrevChunk;
  Chunk_T oNextChunk;
  Chunk_T oNextNextChunk;
  size_t uUnits;

  if (uBytes == 0)
    return NULL;

  /* Step 1: Initialize the heap manager if this is the first call. */
  if (oHeapStart == NULL)
  {
    oHeapStart = (Chunk_T)sbrk(0);
    oHeapEnd = oHeapStart;
  }

  assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));

  /* Step 2: Determine the number of units the new chunk should
      contain. */
  uUnits = Chunk_bytesToUnits(uBytes);

  /* Step 3: For each chunk in the free list... */
  for (oChunk = oFreeList;
       oChunk != NULL;
       oChunk = Chunk_getNextInList(oChunk))
  {
    /* If oChunk is big enough, then use it. */
    if (Chunk_getUnits(oChunk) >= uUnits)
    {
      oChunk = HeapMgr_useChunk(oChunk, uUnits);
      assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));
      return Chunk_toPayload(oChunk);
    }
  }

  /* Step 4: No suitable chunks: 
      Ask the OS for more memory, and create a new chunk (or
      expand the existing chunk) at the end of the free list. */
  oChunk = HeapMgr_getMoreMemory(oPrevChunk, uUnits);
  if (oChunk == NULL)
  {
    assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));
    return NULL;
  }

  /* Step 5: oChunk is big enough, so use it. */
  oChunk = HeapMgr_useChunk(oChunk, uUnits);
  assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));
  return Chunk_toPayload(oChunk);
}

/*--------------------------------------------------------------------*/

void HeapMgr_free(void *pv)
{
  Chunk_T oChunk;
  Chunk_T oldHead;
  Chunk_T oNextChunk;
  Chunk_T oPrevChunk;

  assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));

  if (pv == NULL)
    return;

  oChunk = Chunk_fromPayload(pv);

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* Step 1: Set the status of the given chunk to CHUNK_FREE. */
  Chunk_setStatus(oChunk, CHUNK_FREE);

  /* Step 2: Insert oChunk into the free list at the front. */
  oldHead = Chunk_getNextInList(oFreeList);
  oFreeList = oChunk;
  Chunk_setNextInList(oChunk, oldHead);

  oNextChunk = Chunk_getNextInMem(oChunk, oHeapEnd);
  oPrevChunk = Chunk_getPrevInMem(oChunk, oHeapStart);
  /* Step 3: If appropriate, coalesce the given chunk and the next
      one. */
  if (oNextChunk != NULL)
    if (Chunk_getNextInMem(oChunk, oHeapEnd) == oNextChunk)
    {
      Chunk_setUnits(oChunk,
                     Chunk_getUnits(oChunk) + Chunk_getUnits(oNextChunk));
      Chunk_setNextInList(oChunk,
                          Chunk_getNextInList(oNextChunk));
    }

  /* Step 4: If appropriate, coalesce the given chunk and the previous
      one. */
  if (oPrevChunk != NULL)
    if (Chunk_getNextInMem(oPrevChunk, oHeapEnd) == oChunk)
    {
      Chunk_setUnits(oPrevChunk,
                     Chunk_getUnits(oPrevChunk) + Chunk_getUnits(oChunk));
      Chunk_setNextInList(oPrevChunk,
                          Chunk_getNextInList(oChunk));
    }

  assert(Checker_isValid(oHeapStart, oHeapEnd, oFreeList));
}
