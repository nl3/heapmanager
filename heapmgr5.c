/*--------------------------------------------------------------------*/
/* heapmgr5.c                                                         */
/* Author: Gi Hong Ann, Nicholas Liu                                  */
/*--------------------------------------------------------------------*/

#define _GNU_SOURCE

#include <assert.h>
#include "heapmgr.h"
#include "checker5.h"
#include "chunk5.h"
#include <stddef.h>
#include <unistd.h>
#include <stdio.h>

/* Minimum size of free chunk to be left, eligible for split */
enum
{
  SPLIT_THRESHOLD = 3
};
/* Max number of bins */
enum
{
  MAX_BIN_COUNT = 1023
};

/*--------------------------------------------------------------------*/

/* The state of the HeapMgr. */

/* The address of the start of the heap. */
static Chunk_T oHeapStart = NULL;

/* The address immediately beyond the end of the heap. */
static Chunk_T oHeapEnd = NULL;

/* The free list is a array of all free lists. */
static Chunk_T aoBins[MAX_BIN_COUNT];

/* The number of bins? */
static int iBinCount = MAX_BIN_COUNT;

/*--------------------------------------------------------------------*/

/* Request more memory from the operating system -- enough to store
   uUnits units. Create a new chunk and return it. */

static Chunk_T HeapMgr_getMoreMemory(size_t uUnits)
{
  const size_t MIN_UNITS_FROM_OS = 512;
  Chunk_T oChunk;
  Chunk_T oNewHeapEnd;
  size_t uBytes;

  if (uUnits < MIN_UNITS_FROM_OS)
    uUnits = MIN_UNITS_FROM_OS;

  /* Move the program break. */
  uBytes = Chunk_unitsToBytes(uUnits);
  oNewHeapEnd = (Chunk_T)((char *)oHeapEnd + uBytes);
  if (oNewHeapEnd < oHeapEnd) /* Check for overflow */
    return NULL;
  if (brk(oNewHeapEnd) == -1) /* Return NULL if OS refuses */
    return NULL;

  /* Create a new chunk using that memory */
  oChunk = oHeapEnd;
  oHeapEnd = oNewHeapEnd;

  /* Set the fields of the new chunk. */
  Chunk_setUnits(oChunk, uUnits);
  Chunk_setNextInList(oChunk, NULL);
  Chunk_setPrevInList(oChunk, NULL);

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  return oChunk;
}

/*--------------------------------------------------------------------*/

/* Remove oChunk from free list and return the oChunk.
   Status will be updated outside of this function. 
 */

static Chunk_T HeapMgr_removeFromList(Chunk_T oChunk, Chunk_T oFreeList)
{

  Chunk_T oPrevChunk; /* previous chunk in the list */
  Chunk_T oNextChunk; /* next chunk in the list */
  Chunk_T oNewFront;  /* chunk for new front if front is to be gone */

  assert(oFreeList != NULL); /* can't remove from empty list */
  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* There are two things to consider: location and length of list: 
      length: 0, 1, 2, 3>= 
      location: front, middle, end 
      Note: case list length = 0 is covered by assert above.
   */

  /* case 1) remove chunk in the front */
  if (oChunk == oFreeList)
  {
    oNewFront = Chunk_getNextInList(oChunk);

    /* remove chunk in the front && 
         list length is 1 */
    if (oNewFront == NULL)
    {
      oFreeList = NULL; /* List becomes length zero */
      return oChunk;
    }

    /* remove chunk in the front && 
         list length = 2  
         list length >= 3 is same. 
      */

    oFreeList = oNewFront;
    Chunk_setPrevInList(oFreeList, NULL); /* To keep invariant */
    Chunk_setNextInList(oChunk, NULL);    /* cut conenction to next*/

    return oChunk;
  }

  oPrevChunk = Chunk_getPrevInList(oChunk);
  oNextChunk = Chunk_getNextInList(oChunk);

  /* All cases below are for free list length >=2 */
  assert((oNextChunk != NULL) || (oPrevChunk != NULL));

  /* remove chunk in the back && list length >= 2 */

  if (oNextChunk == NULL)
  {
    Chunk_setNextInList(oPrevChunk, NULL);
    Chunk_setPrevInList(oChunk, NULL);
    return oChunk;
  }

  /* remove chunk in the middle && lits length >= 3 */

  Chunk_setNextInList(oPrevChunk, oNextChunk);
  Chunk_setPrevInList(oNextChunk, oPrevChunk);

  Chunk_setNextInList(oChunk, NULL);
  Chunk_setPrevInList(oChunk, NULL);

  /* check whether oChunk is valid */

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  return oChunk;
}

/*--------------------------------------------------------------------*/

/* Split a chunk into front with length = uUnits and tail with 
   oChunk size - uUnits, Return Tail, Status bit for two chunks are 
   to be set outside of this function as per client HeapMgr_Malloc.
*/
static Chunk_T HeapMgr_splitReturnTail(Chunk_T oChunk, size_t uUnits)
{
  Chunk_T oTail = NULL;
  size_t uBytes;
  size_t uTotalUnits;

  /* Check whether argument oChunk is valid */
  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* Check whether the function is called for chunk with right size */

  uTotalUnits = Chunk_getUnits(oChunk);
  assert(uTotalUnits >= uUnits + (size_t)SPLIT_THRESHOLD);

  /* find the address of oTail */
  uBytes = Chunk_unitsToBytes(uUnits);
  oTail = (Chunk_T)((char *)oChunk + uBytes);

  /* set unit for oTail */
  Chunk_setUnits(oTail, uTotalUnits - uUnits);

  /* set unit for front */
  Chunk_setUnits(oChunk, uUnits);

  /* Are each chunk valid by itself? */
  assert(Chunk_isValid(oTail, oHeapStart, oHeapEnd));
  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* Are they contiguous in memory even after split? */
  assert(Chunk_getNextInMem(oChunk, oHeapEnd) == oTail);
  assert(Chunk_getPrevInMem(oTail, oHeapStart) == oChunk);

  /* Are their lengths summing up to total length? */
  assert(uTotalUnits == Chunk_getUnits(oChunk) +
                            Chunk_getUnits(oTail));

  return oTail;
}

/*--------------------------------------------------------------------*/

/* Add oChunk to the front of free list, assuming its status bit
   is set correctly. It will be set outside of this function as per
   client HeapMgr_Malloc() */

static void HeapMgr_addToFreeList(Chunk_T oChunk)
{

  Chunk_T oOldFront;
  size_t uIndex = Chunk_getUnits(oChunk);
  Chunk_T oFreeList;
  assert(oChunk != NULL);
  if (uIndex > MAX_BIN_COUNT)
  {
    uIndex = MAX_BIN_COUNT;
  }
  oFreeList = aoBins[uIndex];
  /* Is the function called for valid Chunk? */
  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* Cut the connection of oChunk */
  Chunk_setNextInList(oChunk, NULL);
  Chunk_setPrevInList(oChunk, NULL);

  /* if oFreeList is NULL, initialize free list */
  if (oFreeList == NULL)
  {
    oFreeList = oChunk;
    return;
  }

  /* Otherwise, insert Ochunk at front and update oOldFront */
  oOldFront = oFreeList;
  oFreeList = oChunk;
  Chunk_setNextInList(oFreeList, oOldFront);
  Chunk_setPrevInList(oFreeList, NULL);
  Chunk_setPrevInList(oOldFront, oFreeList);

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  return;
}

/*--------------------------------------------------------------------*/

/* Coalesce oChunk with previous chunk in "memory,"
   Return oChunk, the new coalesced chunk,
   adding it to the proper bin. oFreeList is the bin oChunk is 
   currently in.
*/

static Chunk_T HeapMgr_coalesceWithPrev(Chunk_T oChunk)
{

  Chunk_T oPrevChunk = NULL;
  size_t uTotalUnits;
  size_t uChunkUnits;
  size_t uPrevUnits;

  /* Is the function called on valid Chunk ?*/

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  oPrevChunk = Chunk_getPrevInMem(oChunk, oHeapStart);
  assert(Chunk_isValid(oPrevChunk, oHeapStart, oHeapEnd));
  assert(Chunk_getStatus(oPrevChunk) == CHUNK_FREE);

  /* compute uTotalUnit */

  uPrevUnits = Chunk_getUnits(oPrevChunk);
  uChunkUnits = Chunk_getUnits(oChunk);
  uTotalUnits = uPrevUnits + uChunkUnits;

  /* Remove from the bins they are in */
  (void)HeapMgr_removeFromList(oChunk, aoBins[uChunkUnits]);
  (void)HeapMgr_removeFromList(oPrevChunk, aoBins[uPrevUnits]);

  /* Set the address of oChunk to the new earlier one in mem */
  oChunk = oPrevChunk;

  /* Adjust the new chunk */

  Chunk_setUnits(oChunk, uTotalUnits);
  Chunk_setStatus(oChunk, CHUNK_FREE);

  /* Add the new chunk back to the front of the proper bin */
  if (uTotalUnits > MAX_BIN_COUNT)
  {
    uTotalUnits = MAX_BIN_COUNT;
  }
  HeapMgr_addToFreeList(oChunk);

  return oChunk;
}

/*--------------------------------------------------------------------*/

/* Coalesce oChunk with the next free chunk in memory and returns 
 the address of coalesced memory chunk.*/

static Chunk_T HeapMgr_coalesceWithNext(Chunk_T oChunk)
{

  Chunk_T oNextChunk = NULL;
  size_t uTotalUnits;
  size_t uChunkUnits;
  size_t uNextUnits;

  /* Is the function called on valid Chunk ?*/
  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  oNextChunk = Chunk_getNextInMem(oChunk, oHeapEnd);
  assert(Chunk_isValid(oNextChunk, oHeapStart, oHeapEnd));
  assert(Chunk_getStatus(oNextChunk) == CHUNK_FREE);

  /* compute uTotalUnit */
  uNextUnits = Chunk_getUnits(oNextChunk);
  uChunkUnits = Chunk_getUnits(oChunk);
  uTotalUnits = uNextUnits + uChunkUnits;

  /* Remove from the bins they are in */
  /* Need to make sure that free list does not become length zero */
  (void)HeapMgr_removeFromList(oChunk, aoBins[uChunkUnits]);
  (void)HeapMgr_removeFromList(oNextChunk, aoBins[uNextUnits]);

  /* Adjust the new chunk */

  Chunk_setUnits(oChunk, uTotalUnits);
  Chunk_setStatus(oChunk, CHUNK_FREE);

  /* Add the new chunk back to the front of the proper bin */
  if (uTotalUnits > MAX_BIN_COUNT)
  {
    uTotalUnits = MAX_BIN_COUNT;
  }
  HeapMgr_addToFreeList(oChunk);

  return oChunk;
}

/*--------------------------------------------------------------------*/

void *HeapMgr_malloc(size_t uBytes)
{
  Chunk_T oChunk = NULL;
  Chunk_T oTail = NULL;
  size_t startBin = 0;
  size_t curBin = 0;
  size_t uUnits;
  size_t newChunkBin;

  if (uBytes == 0)
    return NULL;

  /* Step 1: Initialize the heap manager if this is the first call. */
  if (oHeapStart == NULL)
  {
    oHeapStart = (Chunk_T)sbrk(0);
    oHeapEnd = oHeapStart;
  }

  assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));

  /* Step 2: Determine the number of units the new chunk should
      contain. */
  uUnits = Chunk_bytesToUnits(uBytes);

  /* Step 2.5: Determine the proper start bin */
  if (uUnits > MAX_BIN_COUNT)
  {
    startBin = MAX_BIN_COUNT;
  }
  else
  {
    startBin = uUnits;
  }

  /* Step 3: For each bin from the proper start bin to the last bin... */

  for (curBin = startBin;
       curBin <= MAX_BIN_COUNT;
       curBin++)
  {
    /* For each chunk in the current bin */
    for (oChunk = aoBins[curBin];
         oChunk != NULL;
         oChunk = Chunk_getNextInList(oChunk))
    {
      if (Chunk_getUnits(oChunk) >= uUnits)
      {
        /* If oChunk size >= uUnits && remaining size < SPLIT_THRESHOLD
            then, 1) remove from the list, 2) set its status to INUSE */

        if (Chunk_getUnits(oChunk) - uUnits < SPLIT_THRESHOLD)
        {
          oChunk = HeapMgr_removeFromList(oChunk, aoBins[curBin]);
          Chunk_setStatus(oChunk, CHUNK_INUSE);
        }
        /* if oChunk size >= uUnits + SPLIT_THRESHOLD */
        /* too big, needs to be split */
        else
        {
          /* remove from the list */
          oChunk = HeapMgr_removeFromList(oChunk, aoBins[curBin]);

          /* split the chunk */
          oTail = HeapMgr_splitReturnTail(oChunk, uUnits);

          /* Insert tail into front of proper bin */
          Chunk_setStatus(oTail, CHUNK_FREE);
          HeapMgr_addToFreeList(oTail);

          /* Set the status bit of front end to CHUNK_INUSE */
          Chunk_setStatus(oChunk, CHUNK_INUSE);

          /* Check validity of heap before return */
        }
        assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));
        return Chunk_toPayload(oChunk);
      }
    }
  }
  /* Step 4: Ask the OS for more memory - enough for new chunk */
  oChunk = HeapMgr_getMoreMemory(uUnits);

  /* Return NULL if OS refuses */
  if (oChunk == NULL)
  {
    assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));
    return NULL;
  }

  /* If not, insert the new Chunk into the proper bin at the front */
  Chunk_setStatus(oChunk, CHUNK_FREE);
  /* Constrain size */
  newChunkBin = Chunk_getUnits(oChunk);
  if (newChunkBin > MAX_BIN_COUNT)
  {
    newChunkBin = MAX_BIN_COUNT;
  }
  HeapMgr_addToFreeList(oChunk);
  assert(aoBins[newChunkBin] != NULL);
  /* If appropriate, coalesce the new Chunk and the previous 
      one in memory */

  if (Chunk_getPrevInMem(oChunk, oHeapStart) != NULL &&
      Chunk_getStatus(Chunk_getPrevInMem(oChunk, oHeapStart)) == CHUNK_FREE)
  {
    printf("Co\n");
    oChunk = HeapMgr_coalesceWithPrev(oChunk);
    /* constrain size */
    newChunkBin = Chunk_getUnits(oChunk);
  }

  /* Step 5: If the current chunk (oChunk) is close to the requested size
      then use it. 
    */
  assert(oChunk != NULL);
  printf("size %d\n", (int)Chunk_getUnits(oChunk));
  printf("size %d\n", (int)newChunkBin);
  assert(aoBins[newChunkBin] != NULL);
  if (newChunkBin > MAX_BIN_COUNT)
  {
    newChunkBin = MAX_BIN_COUNT;
  }

  if (Chunk_getUnits(oChunk) >= uUnits &&
      Chunk_getUnits(oChunk) - uUnits < SPLIT_THRESHOLD)
  {
    printf("Hello");
    oChunk = HeapMgr_removeFromList(oChunk, aoBins[newChunkBin]);
    printf("World");
    Chunk_setStatus(oChunk, CHUNK_INUSE);
  }
  else /* if current chunk is too big */
  {
    /* remove from the list */
    oChunk = HeapMgr_removeFromList(oChunk, aoBins[newChunkBin]);

    /* split the chunk */
    oTail = HeapMgr_splitReturnTail(oChunk, uUnits);

    /* Set tail as free status */
    Chunk_setStatus(oTail, CHUNK_FREE);
    /* Constrain size */
    newChunkBin = Chunk_getUnits(oTail);
    if (newChunkBin > MAX_BIN_COUNT)
    {
      newChunkBin = MAX_BIN_COUNT;
    }
    /* Insert tail into front of proper bin */
    HeapMgr_addToFreeList(oTail);

    /* Set the status bit of front end to CHUNK_INUSE */
    Chunk_setStatus(oChunk, CHUNK_INUSE);
  }

  assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));

  /* Return Payload */
  return Chunk_toPayload(oChunk);
}

/*--------------------------------------------------------------------*/

void HeapMgr_free(void *pv)
{
  Chunk_T oChunk = NULL; /* Chunk that owns memory address in pv */
  int chunkSize = 0;

  assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));

  /*assert(pv != NULL);*/

  oChunk = Chunk_fromPayload(pv);

  assert(Chunk_isValid(oChunk, oHeapStart, oHeapEnd));

  /* (1) Set the status of the given chunk to Free */
  Chunk_setStatus(oChunk, CHUNK_FREE);

  /* (2) Insert the given chunk into the proper bin */
  chunkSize = Chunk_getUnits(oChunk);
  if (chunkSize > MAX_BIN_COUNT)
  {
    chunkSize = MAX_BIN_COUNT;
  }
  HeapMgr_addToFreeList(oChunk);

  /* (3) If appropriate, coalesce the given chunk and the next one 
      in memory */
  if (Chunk_getNextInMem(oChunk, oHeapEnd) != NULL &&
      Chunk_getStatus(Chunk_getNextInMem(oChunk, oHeapEnd)) == CHUNK_FREE)
  {
    oChunk = HeapMgr_coalesceWithNext(oChunk);
  }
  /* TODO: check chunkSize here again? */
  /* (4) If appropriate, coalesce the given chunk and the prev one
      in memory */
  if ((Chunk_getPrevInMem(oChunk, oHeapStart) != NULL) &&
      Chunk_getStatus(Chunk_getPrevInMem(oChunk, oHeapStart)) == CHUNK_FREE)
  {
    oChunk = HeapMgr_coalesceWithPrev(oChunk);
  }

  assert(Checker_isValid(oHeapStart, oHeapEnd, aoBins, iBinCount));
  return;
}
