/*--------------------------------------------------------------------*/
/* checker4.c                                                         */
/* Author: Gi Hong Ann, Nicholas Liu                                  */
/*--------------------------------------------------------------------*/

#include "checker4.h"
#include <stdio.h>
#include <assert.h>

/* In lieu of a boolean data type. */
enum
{
  FALSE,
  TRUE
};

/*--------------------------------------------------------------------*/

int Checker_isValid(Chunk_T oHeapStart, Chunk_T oHeapEnd,
                    Chunk_T oFreeList)
{
  Chunk_T oChunk;
  Chunk_T oPrevChunk;
  Chunk_T oNextChunk; /* Next chunk in free list */
  Chunk_T oTortoiseChunk;
  Chunk_T oHareChunk;
  Chunk_T oChunkStore; /* Pointer Storage needed for cycle detection */

  /* Do oHeapStart and oHeapEnd have non-NULL values? */
  if (oHeapStart == NULL)
  {
    fprintf(stderr, "The heap start is uninitialized\n");
    return FALSE;
  }
  if (oHeapEnd == NULL)
  {
    fprintf(stderr, "The heap end is uninitialized\n");
    return FALSE;
  }

  /* If the heap is empty, is the free list empty too? */
  if (oHeapStart == oHeapEnd)
  {
    if (oFreeList == NULL)
      return TRUE;
    else
    {
      fprintf(stderr, "The heap is empty, but the list is not.\n");
      return FALSE;
    }
  }

  /* Forward traverse memory */

  for (oChunk = oHeapStart;
       oChunk != NULL;
       oChunk = Chunk_getNextInMem(oChunk, oHeapEnd))

    /* Is the chunk valid? */
    if (!Chunk_isValid(oChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "While forward traversing memory,"
                      "detected a bad chunk\n");
      return FALSE;
    }

  /* Backward traverse memory */

  for (oChunk = Chunk_getPrevInMem(oHeapEnd, oHeapStart);
       /*to locate the last chunk in heap */
       oChunk != NULL;
       oChunk = Chunk_getPrevInMem(oChunk, oHeapStart))

    /* Is the chunk valid? */
    if (!Chunk_isValid(oChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "While backward traversing memory,"
                      "detected a bad chunk\n");
      return FALSE;
    }

  /* Is the list devoid of cycles? Use Floyd's algorithm to find out.
      See the Wikipedia "Cycle detection" page for a description. */

  /* Note that there are two possible cycles with doubly linked list */

  /* First check on Forward Cycle */

  oTortoiseChunk = oFreeList;
  oHareChunk = oFreeList;
  oChunkStore = NULL; /* Initialize Chunk Store as NULL */

  if (oHareChunk != NULL)
    oHareChunk = Chunk_getNextInList(oHareChunk);
  while (oHareChunk != NULL)
  {
    oChunkStore = oHareChunk; /* store the Hare to temp storage */
    if (oTortoiseChunk == oHareChunk)
    {
      fprintf(stderr, "The list has a forward cycle\n");
      return FALSE;
    }

    /* Is Chunk Valid while forwad traversing free list? */

    if (!Chunk_isValid(oHareChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "While forward traversing free list, "
                      "detected a bad chunk\n");
      return FALSE;
    }

    /* Move oTortoiseChunk one step. */
    oTortoiseChunk = Chunk_getNextInList(oTortoiseChunk);
    /* Move oHareChunk two steps, if possible. */
    oHareChunk = Chunk_getNextInList(oHareChunk);

    if (oHareChunk != NULL)
    {
      /* Have we arrived at valid chunk while 
            forward traversing free list? */

      if (!Chunk_isValid(oHareChunk, oHeapStart, oHeapEnd))
      {
        fprintf(stderr, "While forward traversing free list, "
                        "detected a bad chunk\n");
        return FALSE;
      }

      oChunkStore = oHareChunk;
      oHareChunk = Chunk_getNextInList(oHareChunk);
    }
  }

  /* Second check on Backward Cycle */

  oTortoiseChunk = oChunkStore;
  oHareChunk = oChunkStore;

  if (oHareChunk != NULL)
  {
    /* Have we arrived at valid chunk while 
         forward traversing free list? */

    if (!Chunk_isValid(oHareChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "While backward traversing free list,"
                      " detected a bad chunk\n");
      return FALSE;
    }

    oHareChunk = Chunk_getPrevInList(oHareChunk);
  }

  while (oHareChunk != NULL)
  {
    /* Have we arrived at valid chunk 
         while backward traversing free list? */

    if (!Chunk_isValid(oHareChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "While backward traversing free list,"
                      " detected a bad chunk\n");
      return FALSE;
    }

    if (oTortoiseChunk == oHareChunk)
    {
      fprintf(stderr, "The list has a backward cycle\n");
      return FALSE;
    }

    /* Move oTortoiseChunk one step. */
    oTortoiseChunk = Chunk_getPrevInList(oTortoiseChunk);
    /* Move oHareChunk two steps, if possible. */

    oHareChunk = Chunk_getNextInList(oHareChunk);

    if (oHareChunk != NULL)
    {
      /* Is Chunk Valid while forwrad traversing free list? */

      if (!Chunk_isValid(oHareChunk, oHeapStart, oHeapEnd))
      {
        fprintf(stderr, "While backward traversing free list,"
                        " detected a bad chunk\n");
        return FALSE;
      }

      oHareChunk = Chunk_getNextInList(oHareChunk);
    }
  }

  /* Traverse the free list,
      check the chunk in relation to prev one in memory
      and next one in memory */

  oPrevChunk = NULL;
  oNextChunk = NULL;
  for (oChunk = oFreeList;
       oChunk != NULL;
       oChunk = Chunk_getNextInList(oChunk))
  {
    oPrevChunk = Chunk_getPrevInMem(oChunk, oHeapStart);
    oNextChunk = Chunk_getNextInMem(oChunk, oHeapEnd);

    /* Is the chunk that we reached valid? */
    if (!Chunk_isValid(oChunk, oHeapStart, oHeapEnd))
    {
      fprintf(stderr, "Traversing the list detected a bad chunk\n");
      return FALSE;
    }

    /* Does the chunk have correct status bit? */
    /* Invariant: chunk in free list has CHUNK_FREE as status bit */

    if (Chunk_getStatus(oChunk) != CHUNK_FREE)
    {
      fprintf(stderr, "Chunk in free list has CHUNK_INUSE\n");
      return FALSE;
    }

    /* Is the previous free chunk contiguous to current? */
    if ((oPrevChunk != NULL) &&
        (Chunk_getNextInMem(oPrevChunk, oHeapEnd) == oChunk))
    {
      fprintf(stderr, "Contiguous free chunks: the previous"
                      " free chunk is just before the current\n");
      return FALSE;
    }

    /* Is the next free chunk contiguous to current? */
    if ((oNextChunk != NULL) &&
        (Chunk_getPrevInMem(oNextChunk, oHeapStart) == oChunk))
    {
      fprintf(stderr, "Contiguous free chunks: the next"
                      " free chunk is just after the current\n");

      return FALSE;
    }
  }

  /* Is the previous free chunk's next = current chunk &&
         next free chunk's previous = current chunk? */

  oPrevChunk = NULL;
  oNextChunk = NULL;

  for (oChunk = oFreeList; oChunk != NULL;
       oChunk = Chunk_getNextInList(oChunk))
  {
    if (oChunk != oFreeList)
    {
      oPrevChunk = Chunk_getPrevInList(oChunk);
    }
    oNextChunk = Chunk_getNextInList(oChunk);

    /* the next of the previous free chunk */
    if (oPrevChunk != NULL &&
        Chunk_getNextInList(oPrevChunk) != oChunk)
    {
      fprintf(stderr, "Next of previous free chunk "
                      "is not the current chunk\n");
      return FALSE;
    }

    /* the previous of the next free chunk */
    if (oNextChunk != NULL &&
        Chunk_getPrevInList(oNextChunk) != oChunk)
    {
      fprintf(stderr, "Previous of next free chunk "
                      "is not the current chunk\n");
      return FALSE;
    }
  }

  /* For all chunks not in list, do we have one with 
      status bit = CHUNK_FREE? */

  for (oChunk = oHeapStart; oChunk != NULL;
       oChunk = Chunk_getNextInMem(oChunk, oHeapEnd))
  {
    if (Chunk_getStatus(oChunk) == CHUNK_FREE)
    {
      /* need to check if they are indeed in free list */
      Chunk_T oCurrent = oFreeList;
      while (oCurrent != NULL && oCurrent != oChunk)
      {
        oCurrent = Chunk_getNextInList(oCurrent);
      }
      if (oCurrent != oChunk)
      {
        fprintf(stderr, "There is a chunk whose status is "
                        "CHUNK_FREEm but not in free list\n");
        return FALSE;
      }
    }
  }

  return TRUE;
}
